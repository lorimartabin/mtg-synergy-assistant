# Handles loading & processing of card data

# Note that there are a VERY GREAT MANY cards, so we'll have to load only a few
#   at a time to avoid using too much memory. My, isn't this a fun challenge!

import json
import re
import os.path
import urllib.request

import synergies

# Parses an entire file to gather the card data in CardData objects
class CardDataLoader:
    def __init__(self, oracle_file_path):
        self.__oracle_file_path = oracle_file_path
    
    # Sets up the file
    def pre_parse(self):
        self.__stream = open(self.__oracle_file_path, encoding = 'utf-8')

        # Read the beginning bracket, '['
        self.__stream.readline()
    
    # Cleans up after the file
    def post_parse(self):
        self.__stream.close()
    
    # Parses one "chunk" of cards (this is to avoid getting too much)
    #   overhead at once)
    # Returns the chunk
    def parse_all(self):
        carddata = []
        # Parse everything
        while True:
            line = self.__stream.readline()

            # Exit condition: end of file
            if line[0] != ' ':
                break
            # If it's a line with actual data, parse it
            else:
                # Cut the terminating newline
                data = CardData(line[:-1])
                if not data.is_bad_card():
                    carddata.append(data)
        
        return carddata

# Holds the Scryfall data of a single card
class CardData:
    TOKEN_RE_PROGRAM = re.compile('[Tt]oken')

    # Build a CardData from the card's json string
    def __init__(self, json_string):
        # The data will occasionally have trailing commas, so delete that
        #   Otherwise, the json library is liable to throw angry errors at us
        if json_string[len(json_string) - 1] == ',':
            json_string = json_string[:-1]

        # Setting this flag will tell the CardDataLoader not to use this card
        self.__bad_card = False
        
        # Parse the data
        json_data = json.loads(json_string)

        # These attributes always exist
        self.__type_line = json_data['type_line']
        self.__keywords = json_data['keywords']
        self.__name = json_data['name']
        self.__scryfall_id = json_data['id']

        # Download the images if they don't already exist
        image_uris = json_data['image_uris']
        for image_size in ('small', 'normal', 'large'):
            local_path = ('res/' + image_size + '/' +
                self.__scryfall_id + '.jpg')

            if not os.path.isfile(local_path):
                remote_path = image_uris[image_size]
                # Download the file
                urllib.request.urlretrieve(remote_path, local_path)

        try:
            # Only keep required fields
            # Mana cost as a future filtering item & to display
            self.__mana_cost = json_data['mana_cost']
            self.__cmc = json_data['cmc']
            # Oracle text must be parsed to know synergies
            self.__oracle_text = json_data['oracle_text']
        # We need extra context to find errors
        except KeyError:
            # This allows us to find the problem card
            print('Problem Card\'s id:', self.__scryfall_id)
            print('              name:', json_data['name'])
            self.__bad_card = True
        
        # Making synergies from keywords is the simplest way for now
        self.__triggering_synergies = 0
        self.__triggers_synergies = 0

        for keyword in self.__keywords:
            # Get the synergy
            try:
                synergy = synergies.SYNERGIES[keyword.upper()]
                # Find which synergies it gets triggered by
                triggering = synergy.get_triggering_synergies()
                # Find which synergies it triggers
                triggers = synergy.get_triggers_synergies()

                # Set this synergy's flag
                self.__triggering_synergies |= 1 << synergy.get_id()

                # Set the proper flags
                for triggering_synergy in triggering:
                    self.__triggering_synergies |= 1 << triggering_synergy.get_id()
                for triggers_synergy in triggers:
                    self.__triggers_synergies |= 1 << triggers_synergy.get_id()
            except KeyError:
                # This means this keyword doesn't have an attached synergy
                print('Nonexistent keyword {}'.format(keyword))
 
    # Getters
    def get_name(self):
        return self.__name

    def get_mana_cost(self):
        return self.__mana_cost

    def get_cmc(self):
        return self.__cmc

    def get_oracle_text(self):
        return self.__oracle_text
    
    def get_scryfall_id(self):
        return self.__scryfall_id
    
    def is_bad_card(self):
        return self.__bad_card

    def get_triggering_synergies(self):
        return self.__triggering_synergies
        
    def get_triggers_synergies(self):
        return self.__triggers_synergies

    # Setters
    def set_name(self, name):
        self.__name = name

    def set_mana_cost(self, mana_cost):
        self.__mana_cost = mana_cost

    def set_cmc(self, cmc):
        self.__cmc = cmc

    def set_oracle_text(self, oracle_text):
        self.__oracle_text = oracle_text
