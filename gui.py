# gui.py
# Holds all the GUI features, such as the root GUI
#   and the card gui

import tkinter
import time
import math
from PIL import ImageTk, Image

import data
import synergies

class GUI:
    def __init__(self):
        # Updates the card view
        def selected_card_callback(cardframe):
            # Check if this is a different CardFrame from before
            if cardframe != self.__selected_cardframe:
                # We replace the card info
                carddata = cardframe.get_carddata()
                path = 'res/normal/' + carddata.get_scryfall_id() + '.jpg'
                # Image
                self.__card_image = ImageTk.PhotoImage(Image.open(path))
                self.__card_image_label.configure(image = self.__card_image)
                # TODO: Other data

                self.__selected_cardframe = cardframe
        self.selected_card_callback = selected_card_callback

        # Start the tkinter application
        root = tkinter.Tk()
        root.geometry('1280x720')

        # Create the left, right, and center frames
        self.__frame_left = tkinter.Frame(root)
        self.__frame_center = tkinter.Frame(root)
        self.__frame_right = tkinter.Frame(root)
        
        # TODO: Attempt loading cards from cached files

        # TODO: User preferences
        # If it didn't work, generate cards now
        # Make a card data loader
        loader = data.CardDataLoader('res/oracle-cards-reduced.json')
        #loader = data.CardDataLoader('res/oracle-cards-20200813090656.json')

        # Load all chunks for now
        loader.pre_parse()

        self.__card_data = loader.parse_all()

        loader.post_parse()

        # Create CardFrames
        self.__collection_card_list_frame = ScrollableCardFrame(self.__frame_left,
            main_gui = self)
        self.__collection_card_list_frame.set_card_data(self.__card_data)
        self.__collection_card_list_frame.after_idle(self.__collection_card_list_frame.update_card_frames)
        
        self.__collection_card_list_frame.pack(fill = 'y', expand = True)

        # Center pane (synergy selection)
        # Make a ScrollableFrame with ALL checkboxen
        def checkbox_callback():
            flags = 0

            # Lambdas are the worst, >:(
            # We need to loop through every checkbox to see if it's a thing.
            for i_checkbox in range(len(self.__synergy_checklist['checkboxes'])):
                var = self.__synergy_checklist['checkbox_vars'][i_checkbox]
                # Set the flag
                if var.get():
                    flags |= (1 << i_checkbox)
            
            filtered_carddata = []
            # If there are synergies selected, we filter
            if flags > 0:
                print('Synergies selected:', format(flags, 'b'))
                for carddata in self.__card_data:
                    # Find flags
                    carddata_flags = (carddata.get_triggering_synergies() |
                        carddata.get_triggers_synergies())
                    # Check if any of the flags match
                    if carddata_flags & flags:
                        filtered_carddata.append(carddata)
            # Otherwise, every carddata is welcome
            else:
                filtered_carddata = self.__card_data
            
            # Now update the ScrollableFrame's carddata list
            self.__collection_card_list_frame.set_card_data(filtered_carddata)
            self.__collection_card_list_frame.update_card_frames()

        self.__synergy_list_frame = ScrollableFrame(self.__frame_center)
        self.__synergy_list_frame.default_bindings()
        self.__synergy_checklist = {
            'checkboxes': [],
            'checkbox_vars': []
        }

        for synergy_name in synergies.SYNERGIES:
            # Make requisite variables
            var = tkinter.IntVar()
            var.set(0)
            # Make a checkbox
            checkbox = tkinter.Checkbutton(
                self.__synergy_list_frame.scrollable_frame,
                text = synergy_name.lower().capitalize(),
                variable = var,
                command = lambda: checkbox_callback()
            )
            # Set references
            self.__synergy_checklist['checkboxes'].append(checkbox)
            self.__synergy_checklist['checkbox_vars'].append(var)
            # Pack the checkbox
            checkbox.pack(anchor = 'nw')
        
        # Pack the ScrollableFrame
        self.__synergy_list_frame.pack(anchor = 'nw', fill = 'y', expand = True)

        # Right pane (card information)
        self.__selected_cardframe = 0
        self.__card_image = 0
        self.__card_image_label = tkinter.Label(self.__frame_right)
        self.__card_info_label = tkinter.Label(self.__frame_right)
        self.__card_image_label.pack(anchor = 'n')
        self.__card_info_label.pack(fill = 'both', expand = True)

        # Pack
        self.__frame_left.pack(side = 'left',
            anchor = 'nw',
            fill = 'y',
            expand = True)
        self.__frame_center.pack(side = 'left',
            anchor = 'nw',
            fill = 'y',
            expand = True)
        self.__frame_right.pack(side = 'left',
            anchor = 'nw')

        # Start the main loop
        tkinter.mainloop()

class CardFrame(tkinter.Frame):
    HEIGHT = 40
    WIDTH = 600

    def __init__(self, master, card_data, click_callback):
        # Call Frame constructor
        super().__init__(master,
            height = self.HEIGHT,
            width = self.WIDTH,
            background = '#222222')
        
        # Assign other attributes
        self.__master = master
        self.__card_data = card_data

        # Allow it to stay at its own height
        self.pack_propagate(False)

        # Make title & mana subnodes
        self.__label_title = tkinter.Label(self,
            text = card_data.get_name(),
            font = ('Matrix-Bold', 24))
        self.__frame_mana = tkinter.Frame(self)

        # Bindings
        cb = lambda e: click_callback(e, cardframe = self) 
        # NOTE: There might be a more efficient way to do these bindings,
        #   but bind_all didn't work and this is pretty simple
        self.bind('<Button-1>', cb)
        self.__label_title.bind('<Button-1>', cb)
        self.__frame_mana.bind('<Button-1>', cb)

        # Pack them, duh
        self.__label_title.pack(side = 'left', anchor = 'w')
        self.__frame_mana.pack(side = 'right', anchor = 'e')
    
    def get_carddata(self):
        return self.__card_data

# ScrollableFrame comes from https://blog.tecladocode.com/tkinter-scrollable-frames/
class ScrollableFrame(tkinter.Frame):
    def __init__(self, container, *args, **kwargs):
        super().__init__(container, *args, **kwargs)
        self.canvas = tkinter.Canvas(self)
        self.scrollbar = tkinter.Scrollbar(self, orient="vertical", command=self.canvas.yview)
        self.scrollable_frame = tkinter.Frame(self.canvas)

        self.canvas.create_window((0, 0), window=self.scrollable_frame, anchor="nw")

        self.canvas.configure(yscrollcommand=self.scrollbar.set)

        self.canvas.pack(side="left", fill="both", expand=True)
        self.scrollbar.pack(side="right", fill="y")
    
    # Sets up default bindings that would be overridden in extending classes
    def default_bindings(self):
        self.scrollable_frame.bind(
            '<Configure>',
            lambda e: self.canvas.configure(
                scrollregion = self.canvas.bbox('all')
            )
        )

class ScrollableCardFrame(ScrollableFrame):
    def __init__(self, master, *args, main_gui, **kwargs):
        # Call ScrollableFrame's constructor
        super().__init__(master, *args, **kwargs)

        def scrollbar_callback(how, *args):
            self.canvas.yview(how, *args)
            self.after(1, self.update_card_frames)
        
        def cardframe_callback(e, cardframe):
            self.__clicked_cardframe = cardframe
            self.after_idle(self.main_gui.selected_card_callback, cardframe)
        self.cardframe_callback = cardframe_callback

        # Reassign the scrollbar callback
        self.scrollbar.configure(command = scrollbar_callback)

        # For some reason, we need to do this to get the master
        self.main_gui = main_gui

        # Holds clicked CardFrame
        self.__clicked_cardframe = 0

        # For updating CardFrames
        self.__i_loaded_frame_offset = 0

        # Holds CardFrames eventually
        self.__cardframe_container = tkinter.Frame(self.scrollable_frame)
        self.__cardframe_container.pack()
        # List of CardFrame-holding subboxen
        self.__cardframe_holders = []

    def set_card_data(self, data):
        self.__card_data = data

        # Set scrollregion
        self.canvas.configure(scrollregion = (0, 0,
            CardFrame.WIDTH, CardFrame.HEIGHT * len(self.__card_data)))

        # Clear CardFrames DESTROY THEM ALL AHAAHAAHAAAHAA
        for holder in self.__cardframe_holders:
            for cardframe in holder:
                # DESTROY IT ALL
                cardframe.destroy()
            # Destroy the holder
            self.__cardframe_holders.pop(0)
        
        # Reset vars
        self.__i_loaded_frame_offset = 0
        self.canvas.yview_moveto('0.0')
    
    def update_card_frames(self):
        N_CARDFRAMES_PER_HOLDER = 8
        # Find visible CardFrame indices
        # Get canvas height
        canvas_height = self.winfo_height()
        n_visible_frames = math.ceil(canvas_height / CardFrame.HEIGHT) + 1
        n_visible_frame_holders = math.ceil(n_visible_frames / N_CARDFRAMES_PER_HOLDER)
        i_last_scroll_position = len(self.__card_data) - n_visible_frames + 1

        # Find scrollbar position (adds to < 1.0)
        raw_sb_pos = self.scrollbar.get()
        sb_height = raw_sb_pos[1] - raw_sb_pos[0]
        max_sb_pos = 1 - sb_height
        # Avoid annoying divide-by-0 errors
        if max_sb_pos == 0:
            scrollbar_position = 0
        else:
            # We divide by the sum; this puts it in the range [0,1]
            scrollbar_position = raw_sb_pos[0] / max_sb_pos

        # Find 1st visible index
        i_first_viewed_frame = int(
            i_last_scroll_position * scrollbar_position)
        # Find last visible index
        i_last_viewed_frame = i_first_viewed_frame + n_visible_frames

        # Find loaded CardFrame indices
        i_first_loaded_frame = self.__i_loaded_frame_offset
        i_last_loaded_frame = (N_CARDFRAMES_PER_HOLDER * len(self.__cardframe_holders) +
            i_first_loaded_frame - 1)

        # TODO: Since the CardFrame-producing code is the same, make these if statements
        #   set values and then do that stuff in the same spot
        # Check if we need to load CardFrames before
        if i_first_viewed_frame < i_first_loaded_frame:
            # Make extra CardFrame holders before
            # Find amount of new CardFrame holders required
            n_new_holders = math.ceil((i_first_loaded_frame - i_first_viewed_frame)
                / N_CARDFRAMES_PER_HOLDER)
            # This is the offset to the very first new CardData
            i_first_carddata_offset = (self.__i_loaded_frame_offset -
                N_CARDFRAMES_PER_HOLDER * n_new_holders)
            for i_new_holder in range(n_new_holders):
                new_holder = []
                # This is the offset to the current CardData
                for i_carddata_offset in range(N_CARDFRAMES_PER_HOLDER):
                    # This is the actual carddata index
                    i_carddata = (i_first_carddata_offset +
                        i_new_holder * N_CARDFRAMES_PER_HOLDER +
                        i_carddata_offset)
                    
                    # Load CardFrame into the holder
                    frame = CardFrame(self.__cardframe_container,
                        self.__card_data[i_carddata],
                        self.cardframe_callback)
                    new_holder.append(frame)
                    # Grid pack it
                    frame.grid(
                        row = i_carddata,

                        ipadx = 0,
                        ipady = 0,
                        padx = 0,
                        pady = 0
                    )
                # Add holder for later reference
                self.__cardframe_holders.insert(0, new_holder)
            
            # Destroy extra CardFrame holders from after
            # Find amount of obsolete holders
            n_old_holders = ((i_last_loaded_frame - i_last_viewed_frame)
                // N_CARDFRAMES_PER_HOLDER)
            print('Found', n_old_holders, 'old holders after')
            for i_old_holder in range(n_old_holders):
                old_holder = self.__cardframe_holders.pop(-1)
                # Destroy old CardFrames
                for frame in old_holder:
                    carddata = frame.get_carddata()
                    frame.destroy()
            # Update i_loaded_frame_offset
            self.__i_loaded_frame_offset -= n_new_holders * N_CARDFRAMES_PER_HOLDER
        
        # Check if we need to load CardFrames after
        if i_last_viewed_frame > i_last_loaded_frame:
            # Make extra CardFrame holders after
            # Find amount of new CardFrame holders required
            n_new_holders = math.ceil((i_last_viewed_frame - i_last_loaded_frame)
                / N_CARDFRAMES_PER_HOLDER)
            # This is the offset to the very first new CardData
            i_first_carddata_offset = len(self.__cardframe_holders) * N_CARDFRAMES_PER_HOLDER
            for i_new_holder in range(n_new_holders):
                new_holder = []
                # This is the offset to the current CardData
                for i_carddata_offset in range(N_CARDFRAMES_PER_HOLDER):
                    # This is the actual carddata index
                    i_carddata = (self.__i_loaded_frame_offset + 
                        i_first_carddata_offset +
                        i_new_holder * N_CARDFRAMES_PER_HOLDER +
                        i_carddata_offset)
                    
                    # Sometimes, there won't be enough, so skip it
                    if i_carddata >= len(self.__card_data):
                        continue
                    
                    print('Built a frame for carddata', i_carddata)
                    # Load CardFrame into the holder
                    frame = CardFrame(self.__cardframe_container,
                        self.__card_data[i_carddata],
                        self.cardframe_callback)
                    new_holder.append(frame)
                    # Grid pack it
                    frame.grid(
                        row = i_carddata,

                        ipadx = 0,
                        ipady = 0,
                        padx = 0,
                        pady = 0
                    )
                # Add holder for later reference
                self.__cardframe_holders.append(new_holder)
            
            # Destroy extra CardFrame holders from before
            # Find amount of obsolete holders
            n_old_holders = ((i_first_viewed_frame - i_first_loaded_frame)
                // N_CARDFRAMES_PER_HOLDER)
            for i_old_holder in range(n_old_holders):
                old_holder = self.__cardframe_holders.pop(0)
                # Destroy old CardFrames
                for frame in old_holder:
                    frame.destroy()
            if n_old_holders > 0:
                print('Found', n_old_holders, 'old holders before')
                print('  Old offset:', self.__i_loaded_frame_offset)
            # Update i_loaded_frame_offset
            self.__i_loaded_frame_offset += n_old_holders * N_CARDFRAMES_PER_HOLDER
            print('  New offset:', self.__i_loaded_frame_offset)
        
        # Set padding to scroll it to the right spot
        self.__cardframe_container.configure(pady = CardFrame.HEIGHT * self.__i_loaded_frame_offset)