# Handles all synergy code
# This includes regexes

class Synergy:
    __n_synergies = 0

    def __init__(self, name, type = 'attribute'):
        self.__name = name
        # These lists hold information about how the synergy affects
        #   other cards
        self.__triggering_synergies = []
        self.__triggers_synergies = []
        # Assign a unique id
        self.__id = Synergy.__n_synergies
        Synergy.__n_synergies += 1
    
    # Makes it so this synergy will "trigger" the given synergy
    def add_trigger(self, other):
        # Add other to self
        self.__triggers_synergies.append(other)
        # Add self to other
        other.add_triggering_synergy(self)
        # Return self to allow chaining calls
        return self

    # NOTE: should only be called by other Synergies
    def add_triggering_synergy(self, other):
        # Add other to self
        self.__triggering_synergies.append(other)
        # (Other already added this to itself)

    def get_name(self):
        return self.__name
    
    def get_id(self):
        return self.__id
        
    def get_triggering_synergies(self):
        return self.__triggering_synergies

    def get_triggers_synergies(self):
        return self.__triggers_synergies

SYNERGIES = {
    # MISCELLANEOUS #
    #################

    '+1/+1_COUNTER': Synergy('+1/+1 counter', 'miscellaneous'),
    '-1/-1_COUNTER': Synergy('-1/-1 counter', 'miscellaneous'),

    # PLAYER ACTIONS #
    ##################

    'ATTACH': Synergy('attach', 'player_action'),
    'BOLSTER': Synergy('bolster', 'player_action'),
    'COUNTER': Synergy('counter', 'player_action'),
    'DISCARD': Synergy('discard', 'player_action'),
    'DRAW': Synergy('draw', 'player_action'),
    'EXILE': Synergy('exile', 'player_action'),
    'MILL': Synergy('mill', 'player_action'),
    'SACRIFICE': Synergy('sacrifice', 'player_action'),
    'SCRY': Synergy('scry', 'player_action'),
    'TAP': Synergy('tap', 'player_action'),
    'UNTAP': Synergy('untap', 'player_action'),
    'CAST': Synergy('cast', 'player_action'),
    'GAIN_LIFE': Synergy('gain life', 'player_action'),
    'LOSE_LIFE': Synergy('lose life', 'player_action'),

    # CARD ACTIONS #
    ################

    'ADAPT': Synergy('adapt', 'card_action'),
    'ATTACK': Synergy('attack', 'card_action'),
    'BLOCK': Synergy('block', 'card_action'),
    'DIE': Synergy('die', 'card_action'),
    'EQUIP': Synergy('equip', 'card_action'),
    'EXPLORE': Synergy('explore', 'card_action'),
    'FIGHT': Synergy('fight', 'card_action'),
    'LEVEL_UP': Synergy('level up', 'card_action'),
    'MANIFEST': Synergy('manifest', 'card_action'),
    'MELD': Synergy('meld', 'card_action'),

    # CARD ATTRIBUTES #
    ###################

    'ABSORB': Synergy('absorb'),
    'AFFINITY': Synergy('affinity for'),
    'AFTERLIFE': Synergy('afterlife'),
    'AFTERMATH': Synergy('aftermath'),
    'AMPLIFY': Synergy('amplify'),
    'ANNIHILATOR': Synergy('annihilator'),
    'ASCEND': Synergy('ascend'),
    'AURA_SWAP': Synergy('aura swap'),
    'BANDING': Synergy('banding'),
    'BANDS_WITH_OTHER': Synergy('bands with other'),
    'BATTLE_CRY': Synergy('battle cry'),
    'BESTOW': Synergy('bestow'),
    'BUSHIDO': Synergy('bushido'),
    'BUYBACK': Synergy('buyback'),
    'CASCADE': Synergy('cascade'),
    'CHAMPION': Synergy('champion'),
    'CHANGELING': Synergy('changeling'),
    'CIPHER': Synergy('cipher'),
    'CLASH': Synergy('clash'),
    'CONSPIRE': Synergy('conspire'),
    'CONVOKE': Synergy('convoke'),
    'CREW': Synergy('crew'),
    'CUMULATIVE_UPKEEP': Synergy('cumulative upkeep'),
    'CYCLING': Synergy('cycling'), # typecycling is a subtype
    'DASH': Synergy('dash'),
    'DEATHTOUCH': Synergy('deathtouch'),
    'DEFENDER': Synergy('defender'),
    'DELVE': Synergy('delve'),
    'DETAIN': Synergy('detain'),
    'DEVOUR': Synergy('devour'),
    'DOUBLE_STRIKE': Synergy('double strike'),
    'DREDGE': Synergy('dredge'),
    'ECHO': Synergy('echo'),
    'EMBALM': Synergy('embalm'),
    'ENCHANT': Synergy('enchant'),
    'ENTWINE': Synergy('entwine'),
    'EPIC': Synergy('epic'),
    'EVOLVE': Synergy('evolve'),
    'EVOKE': Synergy('evoke'),
    'EXALTED': Synergy('exalted'),
    'EXERT': Synergy('exert'),
    'EXPLOIT': Synergy('exploit'),
    'EXTORT': Synergy('extort'),
    'FABRICATE': Synergy('fabricate'),
    'FADING': Synergy('fading'),
    'FATESEAL': Synergy('fateseal'),
    'FIRST_STRIKE': Synergy('first strike'),
    'FLANKING': Synergy('flanking'),
    'FLASHBACK': Synergy('flashback'),
    'FLASH': Synergy('flash'),
    'FLIP': Synergy('flip'),
    'FLYING': Synergy('flying'),
    'FORECAST': Synergy('forecast'),
    'FORTIFY': Synergy('fortify'),
    'FRENZY': Synergy('frenzy'),
    'GRAFT': Synergy('graft'),
    'GRAVESTORM': Synergy('gravestorm'),
    'HASTE': Synergy('haste'),
    'HAUNT': Synergy('haunt'),
    'HEXPROOF': Synergy('hexproof'),
    'HORSEMANSHIP': Synergy('horsemanship'),
    'INDESTRUCTIBLE': Synergy('indestructible'),
    'INFECT': Synergy('infect'),
    'JUMP_START': Synergy('jump-start'),
    'KICKER': Synergy('kicker'),
    'LIFELINK': Synergy('lifelink'),
    'LIVING_WEAPON': Synergy('living weapon'),
    'MADNESS': Synergy('madness'),
    'MENACE': Synergy('menace'),
    'MENTOR': Synergy('mentor'),
    'MODULAR': Synergy('modular'),
    'MONSTROSITY': Synergy('monstrosity'),
    'MORPH': Synergy('morph'),
    'MULTIKICKER': Synergy('multikicker'),
    'MUTATE': Synergy('mutate'),
    'OFFERING': Synergy('offering'),
    'OVERLOAD': Synergy('overload'),
    'PERSIST': Synergy('persist'),
    'POISONOUS': Synergy('poisonous'),
    'POPULATE': Synergy('populate'),
    'PROTECTION': Synergy('protection'),
    'PROVOKE': Synergy('provoke'),
    'PROWESS': Synergy('prowess'),
    'PROWL': Synergy('prowl'),
    'RAMPAGE': Synergy('rampage'),
    'REBOUND': Synergy('rebound'),
    'RECOVER': Synergy('recover'),
    'REINFORCE': Synergy('reinforce'),
    'RENOWN': Synergy('renown'),
    'REPLICATE': Synergy('replicate'),
    'RETRACE': Synergy('retrace'),
    'RIOT': Synergy('riot'),
    'RIPPLE': Synergy('ripple'),
    'REACH': Synergy('reach'),
    'SCAVENGE': Synergy('scavenge'),
    'SHADOW': Synergy('shadow'),
    'SOULBOND': Synergy('soulbond'),
    'SOULSHIFT': Synergy('soulshift'),
    'SPECTACLE': Synergy('spectacle'),
    'SPLICE': Synergy('splice'),
    'SPLIT_SECOND': Synergy('split second'),
    'STORM': Synergy('storm'),
    'SUNBURST': Synergy('sunburst'),
    'SUPPORT': Synergy('support'),
    'SURVEIL': Synergy('surveil'),
    'SUSPEND': Synergy('suspend'),
    'TOTEM_ARMOR': Synergy('totem armor'),
    'TRANSFIGURE': Synergy('transfigure'),
    'TRANSFORM': Synergy('transform'),
    'TRANSMUTE': Synergy('transmute'),
    'TRAMPLE': Synergy('trample'),
    'UNDYING': Synergy('undying'),
    'UNEARTH': Synergy('unearth'),
    'UNLEASH': Synergy('unleash'),
    'VANISHING': Synergy('vanishing'),
    'VIGILANCE': Synergy('vigilance'),
    'WITHER': Synergy('wither'),
}

# Add all triggers

# NOTE: This could *potentially* trigger any synergy, so we don't do it
# LEVEL_UP

# TODO: -1/-1 counter synergy?
# - WITHER
# TODO: Synergies based on card type or color
# - AFFINITY
# - BANDS_WITH_OTHER
# - CHAMPION
# - CHANGELING (literally all creature types)
# - CONVOKE (creature cards)
# - EMBALM (zombies)
# - FORTIFY (lands)
# - OFFERING (card-unique)
# - PROWL
# - SOULSHIFT (spirits)
# - SPLICE (specific, card-unique quality)
# -   and generic examples, e.g. "Creatures you control with X get..."
# TODO: Synergies that are basically other synergies
# - DASH (implies haste on a card)
# - DOUBLE_STRIKE (implies first strike?)
# - INFECT (implies wither AND poisonous)
# - RIOT (implies haste OR counter on a card)
# TODO: Synergizes by putting creatures on the battlefield
# - AFTERLIFE
# - FABRICATE
# - POPULATE
# - UNEARTH
# TODO: Synergizes by cards in graveyard
# - DISCARD
# - DREDGE
# - GRAVESTORM
# TODO: Synergizes by playing in opponent's turn
# - FLASH
# TODO: Synergizes by deck searching
# - TRANSFIGURE
# - TRANSMUTE

# +1/+1 counter synergy
SYNERGIES['ADAPT'].add_trigger(SYNERGIES['+1/+1_COUNTER'])
SYNERGIES['EVOLVE'].add_trigger(SYNERGIES['+1/+1_COUNTER'])
SYNERGIES['EXPLORE'].add_trigger(SYNERGIES['+1/+1_COUNTER'])
SYNERGIES['FABRICATE'].add_trigger(SYNERGIES['+1/+1_COUNTER'])
SYNERGIES['GRAFT'].add_trigger(SYNERGIES['+1/+1_COUNTER'])
SYNERGIES['MENTOR'].add_trigger(SYNERGIES['+1/+1_COUNTER'])
SYNERGIES['MODULAR'].add_trigger(SYNERGIES['+1/+1_COUNTER'])
SYNERGIES['MONSTROSITY'].add_trigger(SYNERGIES['+1/+1_COUNTER'])
SYNERGIES['REBOUND'].add_trigger(SYNERGIES['+1/+1_COUNTER'])
SYNERGIES['RENOWN'].add_trigger(SYNERGIES['+1/+1_COUNTER'])
SYNERGIES['SCAVENGE'].add_trigger(SYNERGIES['+1/+1_COUNTER'])
# TODO: Extra context
SYNERGIES['SUNBURST'].add_trigger(SYNERGIES['+1/+1_COUNTER'])
SYNERGIES['SUPPORT'].add_trigger(SYNERGIES['+1/+1_COUNTER'])
SYNERGIES['UNLEASH'].add_trigger(SYNERGIES['+1/+1_COUNTER'])

# Synergizes by players gaining or losing life
SYNERGIES['EXTORT'].add_trigger(
    SYNERGIES['GAIN_LIFE']).add_trigger(
    SYNERGIES['LOSE_LIFE'])
SYNERGIES['LIFELINK'].add_trigger(
    SYNERGIES['GAIN_LIFE']).add_trigger(
    SYNERGIES['LOSE_LIFE'])
SYNERGIES['SPECTACLE'].add_trigger(
    SYNERGIES['LOSE_LIFE'])

SYNERGIES['ANNIHILATOR'].add_trigger(SYNERGIES['SACRIFICE'])
SYNERGIES['BANDING'].add_trigger(SYNERGIES['BANDING'])
SYNERGIES['DELVE'].add_trigger(SYNERGIES['EXILE'])
SYNERGIES['DEVOUR'].add_trigger(SYNERGIES['SACRIFICE'])
SYNERGIES['EVOKE'].add_trigger(SYNERGIES['SACRIFICE'])
SYNERGIES['EXALTED'].add_trigger(SYNERGIES['EXALTED'])
SYNERGIES['EXPLOIT'].add_trigger(SYNERGIES['SACRIFICE'])
SYNERGIES['FADING'].add_trigger(SYNERGIES['SACRIFICE'])
SYNERGIES['RIPPLE'].add_trigger(SYNERGIES['RIPPLE'])
SYNERGIES['VANISHING'].add_trigger(SYNERGIES['SACRIFICE'])
SYNERGIES['SACRIFICE'].add_trigger(SYNERGIES['DIE'])

# Custom (non-definition-based) triggers
# Because knowing cards offers advantage
SYNERGIES['CLASH'].add_trigger(SYNERGIES['SCRY'])
SYNERGIES['CLASH'].add_trigger(SYNERGIES['FATESEAL'])
# Because blocking
SYNERGIES['FLYING'].add_trigger(SYNERGIES['FLYING'])
SYNERGIES['FLYING'].add_trigger(SYNERGIES['REACH'])
SYNERGIES['REACH'].add_trigger(SYNERGIES['FLYING'])
SYNERGIES['HORSEMANSHIP'].add_trigger(SYNERGIES['HORSEMANSHIP'])
SYNERGIES['SHADOW'].add_trigger(SYNERGIES['SHADOW'])
# Because forced blocking
SYNERGIES['PROVOKE'].add_trigger(SYNERGIES['DEATHTOUCH'])
SYNERGIES['DEATHTOUCH'].add_trigger(SYNERGIES['PROVOKE'])
# Because advantageous blocking
SYNERGIES['FLANKING'].add_trigger(SYNERGIES['FLANKING'])