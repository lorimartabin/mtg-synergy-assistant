# README

MTG Synergy Assistant is an abandoned tool for finding synergies between
Magic: The Gathering cards. It downloads pictures and shows them in a
scrollable list. You can even click a card for a closeup view.

It's built in Python 3 from relative scratch, so it's super cool.

## Setup

1. Download or clone repository
2. Install ImageTk for Python 3
3. Run main.py using Python 3 to start application
4. Ignore bugs

